import React, { Component } from 'react';

export class Post extends Component {
  state = {
    isMouseOver: false,
  };

  onDelete = () => {
    const { post, deletePost } = this.props;
    deletePost(post.id);
  };

  onMouseEnter = () => this.setState({ isMouseOver: true });

  onMouseLeave = () => this.setState({ isMouseOver: false });

  render() {
    const {
      post: { id, text, title },
    } = this.props;
    const { isMouseOver } = this.state;
    return (
      <div
        className={['post-wrap', isMouseOver && 'post-wrap-selected']
          .filter(Boolean)
          .join(' ')}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <h3 className='post-title' onClick={() => alert(title)}>{title}</h3>
        <div>
          #{id} - {text}
        </div>
        <div>
          <button onClick={this.onDelete}>X</button>
          <span>{text.length > 10 ? 'Post is long' : 'Post is short'}</span>
        </div>
      </div>
    );
  }
}
